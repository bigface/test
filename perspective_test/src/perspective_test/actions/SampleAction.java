package perspective_test.actions;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.internal.workbench.E4XMIResourceFactory;
import org.eclipse.e4.ui.internal.workbench.ModelServiceImpl;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.descriptor.basic.MPartDescriptor;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspective;
import org.eclipse.e4.ui.model.application.ui.advanced.MPerspectiveStack;
import org.eclipse.e4.ui.model.application.ui.basic.MWindow;
import org.eclipse.e4.ui.model.application.ui.basic.MWindowElement;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.IPerspectiveRegistry;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.WorkbenchPlugin;

public class SampleAction implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;

	IEclipseContext context = (IEclipseContext) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getService(IEclipseContext.class);
	EModelService modelService = context.get(EModelService.class);
	MApplication application = (MApplication) PlatformUI.getWorkbench().getService(MApplication.class);

	public SampleAction() {
	}

	

	
	public void run2(IAction action) {
		List<MWindow> windows = modelService.findElements(application, null, MWindow.class, null);
		MWindow window = windows.get(0);
		
		EPartService partService = context.get(EPartService.class);

		final MPerspective perspective = modelService.getActivePerspective(window);

		final MUIElement snippet = modelService.cloneSnippet(application, perspective.getElementId(), window);
		if (snippet != null) {
			snippet.setToBeRendered(true);
			MElementContainer<MUIElement> parent = perspective.getParent();
			perspective.setToBeRendered(false);

			List<MWindow> existingDetachedWindows = new ArrayList<MWindow>();
			existingDetachedWindows.addAll(perspective.getWindows());

			MPerspective dummyPerspective = (MPerspective) snippet;
			while (dummyPerspective.getWindows().size() > 0) {
				MWindow detachedWindow = dummyPerspective.getWindows().remove(0);
				perspective.getWindows().add(detachedWindow);
			}

			parent.getChildren().remove(perspective);

			parent.getChildren().add(snippet);
			System.out.println(parent.getChildren().get(0).getElementId());
			partService.switchPerspective((MPerspective) snippet);
		}
	}
	
	public void run1(IAction action) {
		/*
		 * IPerspectiveDescriptor[] pds =
		 * PlatformUI.getWorkbench().getPerspectiveRegistry().getPerspectives();
		 * for (IPerspectiveDescriptor pd : pds) {
		 * System.out.println(pd.getLabel()); }
		 */

		String path = "C:" + File.separator + "temp" + File.separator + "eclipse_pespective_test.xmi";
		File f = new File(path);
		FileOutputStream fos = null;

		System.out.println("****************************************************");

		// Find objects by type - can only find ACTIVE perspectives
		List<MPerspective> activePerspectives = modelService.findElements(application, null, MPerspective.class, null);
		System.out.print("Found parts(s) : " + activePerspectives.size() + " ===  ");
		for (int i = 0; i < activePerspectives.size(); i++) {
			System.out.print(activePerspectives.get(i).getLabel() + "  :::  ");
		}
		System.out.println("\n---------------------------------------------------");

		// TEST try to find all perspectives whether they are active or not
		/*
		int searchTag = EModelService.OUTSIDE_PERSPECTIVE | EModelService.GLOBAL | EModelService.IN_ANY_PERSPECTIVE;
		List<MPerspective> els = modelService.findElements(application, MPerspective.class, searchTag, new MSelec());
		System.out.print("Found parts(s) : " + els.size() + " >>>  ");
		for (int i = 0; i < els.size(); i++) {
			System.out.print(els.get(i).getElementId() + "  :::  ");
		}
		System.out.println("---------------------------------------------------");
		*/


		// ///////////////////////////////////////////////////
		// TEST - find specific pespective by label, seems only returns the
		// reference if the perspective is ACTIVE...
		/*
		IPerspectiveRegistry reg = WorkbenchPlugin.getDefault().getPerspectiveRegistry();
		String label = "Git";
		IPerspectiveDescriptor persp = reg.findPerspectiveWithLabel(label);
		String id = persp.getId();
		System.out.println("Git id=" + id);
		MUIElement element = modelService.find(id, application);


		
		
		
		
		//TEST 
		MPerspective mp_git = (MPerspective) element;
		
		List<MWindow> win2 = modelService.findElements(application, null, MWindow.class, null);
		
		MPerspective mp_exist = (MPerspective) modelService.find(id, win2.get(0));
		
		if(mp_exist!=null){
			MPerspective mp_cloned = (MPerspective) modelService.cloneElement(mp_git, application);
			mp_cloned.setToBeRendered(true);
			//modelService.removePerspectiveModel(exPerpective, win2.get(0));
			
			MElementContainer<MUIElement> perspContainer = modelService.getActivePerspective(win2.get(0)).getParent();
			perspContainer.getChildren().remove(mp_exist);
			mp_cloned.setLabel("Git_New");
			
			System.out.println("mp_exist data ==>" + mp_exist.getContainerData());
			System.out.println("mp_cloned data ==>" + mp_cloned.getContainerData());
			perspContainer.getChildren().add(mp_cloned);

//			List<MPerspectiveStack> pstk = modelService.findElements(application, null, MPerspectiveStack.class, null);
//			pstk.get(0).getChildren().remove(mp_exist);
//			pstk.get(0).getChildren().add(mp_cloned);
		}
		
		*/
		
		
		
		
			
		printSnippetsFromApplication();
		
/*		
		String MAIN_PERSPECTIVE_STACK_ID = "org.eclipse.ui.ide.perspectivestack";
		// String id_perspective_minimized =
		// "org.eclipse.ui.ide.perspectivestack(minimized)";
		MPerspectiveStack perspectiveStack = (MPerspectiveStack) modelService.find(MAIN_PERSPECTIVE_STACK_ID, application);
		// MUIElement perspectiveStackMinimized =
		// modelService.find(id_perspective_minimized, application);
		List<MPerspective> allp = perspectiveStack.getChildren();
*/
		
		
		
		
		
		

		/******************************************/
		/* Save to file                           */
		/******************************************/
		/*
		try {
			fos = new FileOutputStream(f);
			savePerspectives(activePerspectives, fos);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				fos = null;
			}
		}
		*/

		
		
		
		//====================================================
		// Load from a file...
		//====================================================
		
		
		FileInputStream fis = null;
		List<EObject> savedPerspectives = null;
		try {
			fis = new FileInputStream(f);
			savedPerspectives = loadPerspective(fis);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				fis = null;
			}
		}


		
		System.out.println("<< Saved Perspective Imported... >>");
		
		MPerspective new_perspective = null;
		
		if (savedPerspectives != null) {
			for (EObject pers : savedPerspectives) {
				MPerspective loadedPerspective = (MPerspective) pers;
				System.out.print(" >>>  " + loadedPerspective.getLabel());
				
				
				if(loadedPerspective.getLabel().equals("RDAL_1")){
					new_perspective = loadedPerspective;
				}
			}

			
			
			List<MWindow> windows = modelService.findElements(application, null, MWindow.class, null);
			if (windows.size() > 0) {
				MElementContainer<MUIElement> persContainer = modelService.getActivePerspective(windows.get(0)).getParent();

				List<MUIElement> pch = persContainer.getChildren();
				System.out.println("");

				// remove selected perspective
//				MPerspective p2 = (MPerspective) savedPerspectives.get(1);
				
//				MPerspective existingPerpective = (MPerspective) modelService.find(p2.getElementId(), windows.get(0));
//				if(existingPerpective!=null){
//				modelService.resetPerspectiveModel(existingPerpective, windows.get(0));
//				modelService.removePerspectiveModel(existingPerpective, windows.get(0));
//				}
				
				
				
				// replace the save perspective.
				//persContainer.getChildren().add(p2);
			
				
			List<MPerspectiveStack> pstk = modelService.findElements(application, null, MPerspectiveStack.class, null);
			pstk.get(0).getChildren().add(new_perspective);	
			
			
			//	EPartService partService = context.get(EPartService.class);
//				partService.switchPerspective(p2);
				
				
				
			}
			

			
	
		}
		

	}


	private void printSnippetsFromApplication() {
		//TEST
		List<MUIElement> snippets = application.getSnippets();
		System.out.print("All Snippet From Application ==> ");
		for(MUIElement s:snippets)
		{
			System.out.print("  <<>>  " + s.getElementId());
		}
		System.out.println("");
	}

	public void savePerspectives(List<MPerspective> selectedPerspectives, FileOutputStream fos) {
		/*
		 * Save the MPerspective as EMF resource
		 */
		// create a resource, which is able to store e4 model elements
		E4XMIResourceFactory e4xmiResourceFactory = new E4XMIResourceFactory();
		Resource resource = e4xmiResourceFactory.createResource(null);

		// must clone the perspective as snippet, otherwise the running
		// application would break, because the saving process of the resource
		// removes the element from the running application model
		for (MPerspective mp : selectedPerspectives) {
			// MPerspective selectedPerspective = selectedPerspectives.get(0);
			MUIElement clonedPerspective = modelService.cloneElement(mp, application);

			// add the cloned model element to the resource so that it may be
			// stored
			resource.getContents().add((EObject) clonedPerspective);
		}

		try {
			resource.save(fos, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public EList<EObject> loadPerspective(FileInputStream fis) {
		// create a resource, which is able to store e4 model elements
		E4XMIResourceFactory e4xmiResourceFactory = new E4XMIResourceFactory();
		Resource resource = e4xmiResourceFactory.createResource(null);

		// load the stored model element
		try {
			resource.load(fis, null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// List<MPerspective> persList= new ArrayList<MPerspective>();

		if (!resource.getContents().isEmpty()) {

			return resource.getContents();
		}

		return null;

	}

	/**
	 * Selection in the workbench has been changed. We can change the state of
	 * the 'real' action here if we want, but this can only happen after the
	 * delegate has been created.
	 * 
	 * @see IWorkbenchWindowActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

	/**
	 * We can use this method to dispose of any system resources we previously
	 * allocated.
	 * 
	 * @see IWorkbenchWindowActionDelegate#dispose
	 */
	public void dispose() {
	}

	/**
	 * We will cache window object in order to be able to provide parent shell
	 * for the message dialog.
	 * 
	 * @see IWorkbenchWindowActionDelegate#init
	 */
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}
}